\documentclass[a4paper]{article}

% -- text --
\usepackage{natbib}
\usepackage[utf8]{inputenc}
\usepackage{xspace}
\usepackage[multiple]{footmisc}
\usepackage{hyperref}

\newcommand{\ie}{i.e.\ }
\newcommand{\eg}{e.g.\ }

% -- math --
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{stmaryrd}

\newcommand{\function}[2]{\mathit{#1}\left(#2\right)}
\newcommand{\vars}[1]{\function{vars}{#1}}
\newcommand{\pre}[1]{\function{pre}{#1}}
\newcommand{\post}[1]{\function{post}{#1}}
\newcommand{\eff}[1]{\function{eff}{#1}}
\newcommand{\effcond}[2]{\function{effcond}{#1, #2}}
\newcommand{\effects}[1]{\function{effects}{#1}}
\newcommand{\resfact}[2]{\function{resulting\_fact}{#1, #2}}
\newcommand{\possible}[3]{\function{possible}{#1, #2, #3}}
\newcommand{\regr}[2]{\function{regr}{#1, #2}}

\newcommand{\atom}[2]{\uppercase{#1} \mapsto #2}
\newcommand{\facteff}[3]{\atom{#1}{#2} \rightarrow \atom{#1}{#3}}
\newcommand{\facteffpair}[3]{\langle \atom{#1}{#2}, \atom{#1}{#3} \rangle}

% -- figures --
\usepackage{graphicx}
\usepackage{contour}
\usepackage{xcolor}
\usepackage{tikz}
	\usetikzlibrary{backgrounds}
	\usetikzlibrary{shapes.geometric}
	
\contourlength{1pt}
\newcommand{\treelabel}[1]{\scriptsize\contour[64]{white}{$p_{#1}$}}

% -- pseudocode --
\usepackage{algorithm}
\usepackage[noend]{algpseudocode}

\makeatletter
\def\BState{\State\hskip-\ALG@thistlm}
\makeatother

% -- todo's --
\usepackage[colorinlistoftodos]{todonotes} % todo's can be turned off by adding "disable" to options

% -- theorems --
\usepackage{amsthm}

\newtheorem{theorem}{Theorem}
\newtheorem{definition}{Definition}

\begin{document}
	\title{\textbf{Cartesian Abstraction}\\Allowing Conditional Effects}
	\author{Clemens Büchner
	}
	\date{\today}
	\maketitle

%\listoftodos

Seipp and Helmert (??) introduced \emph{Cartesian Abstraction} for \emph{planning tasks}.
We define the term planning task accordingly:

\begin{definition}
	\label{def:plan_task}
	A \emph{planning task} is a tuple $\Pi = \langle \mathcal{V}, \mathcal{O}, s_0, s_\star \rangle$, where
	\begin{itemize}
		\item $\mathcal{V} = \langle v_1, \dots, v_n \rangle$ is a finite set of \emph{state variables}, each with an associated finite domain $dom(v_i)$.
		
		An \emph{atom} is a pair $\langle v, d \rangle$, also written $v \mapsto d$, with $v \in \mathcal{V}$ and $d \in dom(v)$.
		
		A \emph{partial state} $s$ is an assignment that maps a subset $\vars{s}$ of $\mathcal{V}$ to values in their respective domains.
		We write $s[v] \in dom(v)$ for the value which $s$ assigns to the variable $v$.
		Partial states defined on all variables are called \emph{states}, and $S(\Pi)$ is the set of all states of $\Pi$.
		We will interchangeably treat partial states as mappings from variables to values or as sets of atoms.
		
		\item $\mathcal{O}$ is a finite set of \emph{operators}.
		Each operator $o$ has a \emph{precondition} $\pre{o}$, an \emph{effect} $\eff{o}$ and a non-negative \emph{cost} $cost(o) \in \mathbb{R}_0^+$.
		The precondition $\pre{o}$ and effect $\eff{o}$ are partial states.
		An operator $o \in \mathcal{O}$ is applicable in state $s$ if $\pre{o} \subseteq s$.
		
		\item $s_0 \in S(\Pi)$ is the \emph{initial state} and $s_\star$ is a partial state, called the \emph{goal}.
	\end{itemize}
\end{definition}

The theory presented in the paper of Seipp and Helmert (??), however, does not allow \emph{conditional effects} of operators.
To enlarge the range of planning tasks which can be solved with the proposed strategy, we want to make it applicable to such cases.
Let us therefore first define effects according to the \emph{Planning and Optimization} lecture of Prof.~Malte~Helmert, held at the University of Basel (e.g. fall term 2017).

\begin{definition}
	\label{def:effect}
	\emph{Effects} over state variables $\mathcal{V}$ are inductively defined as follows:
	\begin{itemize}
		\item If $v \in \mathcal{V}$ is a state variable, then $v \mapsto d \in dom(v)$ is an effect (\emph{atomic effect}).
		\item If $e_1, \dots, e_n$ are effects, then $(e_1 \wedge \dots \wedge e_n)$ is an effect (\emph{conjunctive effect}).
		
		The special case with $n = 0$ is the \emph{empty effect $\top$}.
		\item If $\mathcal{X}$ is a logical formula and $e$ is an effect, then $(\mathcal{X} \vartriangleright e)$ is an effect (\emph{conditional effect}).
	\end{itemize}
\end{definition}

\begin{definition}
	\label{def:effcond}
	Let $\atom{\ell}{dom(v)}$ be an atomic effect.
	The \emph{effect condition} $\effcond{\ell}{e}$ under which $\ell$ triggers given the effect $e$ is a propositional formula defined as follows:
	\begin{itemize}
		\item $\effcond{\ell}{\ell} = \top$
		\item $\effcond{\ell}{\ell'} = \bot$ for atomic effects $\ell' \neq \ell$
		\item $\effcond{\ell}{(e_1 \wedge \dots \wedge e_n)} = \effcond{\ell}{e_1} \vee \dots \vee \effcond{\ell}{e_n}$
		\item $\effcond{\ell}{(\mathcal{X} \vartriangleright e)} = \mathcal{X} \wedge \effcond{\ell}{e}$
	\end{itemize}
\end{definition}

Since we cannot generally tell which value a variable $v$ will have after applying an operator $o$ with conditional effects, we need to update the theory suggested by Seipp and Helmert (??) whenever $\post{o}[v]$ is used (they introduce the \emph{postcondition} $\post{o}$ as the partial state consisting of $\pre{o}$ updated by $\eff{o}$).

For the problem of the Rubik's Cube considered in our studies, there occurs only one special case of conditional effects: For all (conditional) effects triggered by an operator $o$ we never have to check for other variables than the one that is changed by the effect.
We therefore introduce \emph{factorized effect tasks}:

\begin{definition}
	\label{def:fact_opt}
	Operator $o$ is a \emph{factorized effect operator} if $\eff{o}$ has the following form: $\facteff{x}{x_1}{x_2} \wedge \facteff{y}{y_1}{y_2} \wedge \dots \wedge \facteff{z}{z_1}{z_2}$.
	
	We write $\effects{o}$ for the set of fact pairs $\facteffpair{x}{x_1}{x_2}$ where $x_1, x_2 \in dom(X)$, $x_1 \neq x_2$ and $\atom{x}{x_1}$ is the \emph{effect condition} and $\atom{x}{x_2}$ is the \emph{effect fact}.
	A factorized effect operator cannot have two fact pairs with equal effect condition, therefore $x_1 \neq x_3$ for all $\facteffpair{x}{x_1}{x_2}, \facteffpair{x}{x_3}{x_4} \in \effects{o}$.

	We say $v \in \vars{o}$ if either $v \in \vars{\pre{o}}$ or there is a fact pair $\langle v=v_1, v=v_2 \rangle \in \effects{o}$.
	
	Let further $\effects{o}[v] \subseteq \effects{o}$ be the set of fact pairs $\langle v=v_1, v=v_2 \rangle$ that are concerned with variable $v \in \mathcal{V}$.
	The set found by $\effects{o}[v]$ can be the empty set $\emptyset$.
\end{definition}

Let us clarify the meaning of Definition~\ref{def:fact_opt} by the following example:

Let $\Pi = \langle \mathcal{V}, \mathcal{O}, s_0, s_\star \rangle$ be the factorized effect task of counting from zero to three with 
\begin{itemize}
	\item $\mathcal{V} = \{c\}$ where $c$ is the counter variable with $dom(c) = \{0, 1, 2, 3\}$,
	\item $\mathcal{O} = \{\mathit{count}\}$ with \newline $\effects{\mathit{count}} = \{\langle c=0, c=1 \rangle, \langle c=1, c=2 \rangle, \langle c=2, c=3 \rangle\}$, 
	\item $s_0 = \{c \mapsto 0\}$ and $s_\star = \{c \mapsto 3\}$
\end{itemize}

In this example it is impossible to tell the exact value of $c$ after applying $\mathit{count}$, when we do not have any information about the value of $c$ before applying $\mathit{count}$.
Since this is used by Seipp and Helmert (??) by the function $\post{o}[v]$, for CEGAR with unconditional effects, we have to go over their theory and make some minor changes to make it work for problems with conditional effects.

We start by defining \emph{Cartesian sets} and \emph{Cartesian abstraction} according to Seipp and Helmert (??):
\begin{definition}
	\label{def:cartesian}
	A set of states for a planning task with variables $\langle v_1, \dots, v_n \rangle$ is called \emph{Cartesian} if it is of the form $A_1 \times A_2 \times \dots \times A_n$, where $A_i \subseteq dom(v_i)$ for all $1 \leq i \leq n$.
	
	An abstraction is called \emph{Cartesian} if all its abstract states ar Cartesian sets.
	
	For an abstract state $a = A_1 \times \dots \times A_n$, we define $dom(v_i, a) = A_i \subseteq dom(v_i)$ for all $1 \leq i \leq n$ as the set of values that variable $v_i$ can have in abstract state $a$.
\end{definition}

Considering a factorized effect task $\Pi = \langle \mathcal{V}, \mathcal{O}, s_0, s_\star \rangle$, let $a$ be an abstract state in $\Pi$, let $o \in \mathcal{O}$ be a factorized effect operator and let $X \in \mathcal{V}$ be a state variable.
Then the function $\resfact{c}{o}$ computes the fact that is true for $v$ after applying $o$.
\begin{equation}
	\resfact{\atom{X}{x_1}}{o} = \left\{\begin{array}{ll}
		\atom{X}{x_2} & \text{if } \facteffpair{X}{x_1}{x_2} \in \effects{o} \\
		\atom{X}{x_1} & \text{otherwise}
	\end{array}\right.
\end{equation}

We define the function $\possible{a}{o}{v}$ to obtain a set of values that $v$ can possibly have after applying $o$.
\begin{equation}
	\possible{a}{o}{v} = \bigcup_{d \in dom(v, a)} \{\resfact{d}{o}\}
\end{equation}

We go on by working those revisions into the pseudocode provided within the work of Seipp and Helmert (??).
Remember: We have to get rid of all usages of $\post{o}$.
Also this proposition only works for factorized effect tasks.
Algorithm~\ref{lst:check} checks, whether a transition exists between two abstract states through a given operator.

\begin{algorithm}[t]
	\centering
	\input{./Listings/check_transition.tex}
	\caption{Transition check. Returns true iff factorized effect operator $o$ induces at least one transition between abstract states $a$ and $b$.}
	\label{lst:check}
\end{algorithm}

We also need to update the regression described in property \textbf{P4} in the Paper of Seipp and Helmert (??).

\begin{definition}
	\label{def:regr}
	Let $\atom{X}{x_1}$ be an atomic effect and let $o$ be an operator in any planning task.
	Then the regression of $v$ through $o$ is either the effect condition for $\atom{X}{x_1}$ or $\atom{X}{x_1}$ itself, if there is no effect condition that triggers an effect $\atom{X}{x_2}$ where $x_1 \neq x_2$.
	$$
		\regr{\atom{X}{x_1}}{\eff{o}} = \effcond{\atom{X}{x_1}}{\eff{o}} \vee (\atom{X}{x_1} \wedge \neg\effcond{X \neq x_1}{\eff{o}})
	$$ \todo{where should I break the line in this equation to make it not overfill hbox?}
\end{definition}
	
Considering the special case of factorized effect tasks, we can specify the regression to the case of factorized effect operators.
Let now $o$ be such a factorized effect operator, then the regression of an atomic effect $\atom{X}{x_2}$ as described above is the following disjunction:
\begin{equation}
	\regr{\atom{X}{x_2}}{o} = \bigvee_{\substack{\facteffpair{X}{x_1}{x_2} \\ \in \effects{o}}} \atom{X}{x_1} \vee \left(\atom{X}{x_2} \wedge \neg\bigvee_{\substack{\facteffpair{X}{x_3}{x_4} \\ \in \effects{o} : x_2 \neq x_4}}\atom{X}{x_3}\right)
\end{equation}

We can rewrite the disjunction above for better readability by reasoning that either $\atom{X}{x_2}$ occurs as the effect condition of a factorized effect pair $e \in \effects{o}$ or it does not.
This information is contained in the in the term $\neg\bigvee_{\facteffpair{X}{x_3}{x_4} \in\effects{o} : x_2 \neq x_4} \atom{X}{x_3}$:
We check for all factorized effect pairs which do not have $\atom{X}{x_2}$ as their effect fact, that their effect condition is not true, which means that $\atom{X}{x_2}$ does never occur as the effect condition.
If so, we must add the fact $\atom{X}{x_2}$ to the regression of itself through the operator $o$.
We end up making the following case distinction:
\begin{equation}
	\regr{\atom{X}{x_2}}{o} = \bigvee\limits_{\substack{\facteffpair{X}{x_1}{x_2} \\ \in\effects{o}}} \atom{X}{x_1} \vee \left\{\begin{array}{ll}
		\atom{X}{x_2} & \begin{array}{l}
			\text{if } \atom{X}{x_2} \text{ does not} \\ \text{occur as an effect} \\ \text{condition in } o 
		\end{array} \\
		\bot & \text{otherwise}
	\end{array}\right.
\end{equation}

The following example should illustrate this definition:
\begin{equation*}
	\eff{o_1} = \facteff{X}{x_1}{x_2} \wedge \facteff{Z}{z_1}{z_2} \wedge \facteff{Z}{z_2}{z_1}
\end{equation*}
\begin{eqnarray*}
	\regr{\atom{Z}{z_1}}{o_1} & = & \top \wedge \regr{\atom{Z}{z_1}}{\eff{o_1}} \\
	& = & (\atom{Z}{z_2} \vee (\atom{Z}{z_1} \wedge \neg(\atom{Z}{z_1}))) \\
	& = & \atom{Z}{z_2}
\end{eqnarray*}

Since we want to compute the regression not only for one atomic effect but for a whole abstract state, we need to add the following to our definition:

For a set $\mathcal{X} \subseteq dom(X)$ of values for a variable $X$ we define the regression as
\begin{equation}
	\regr{\mathcal{X}}{o} = \bigcup_{x \in \mathcal{X}} \regr{\atom{X}{x}}{o}
\end{equation}
and for an abstract state $a$ as
\begin{equation}
	\regr{a}{o} = A_1 \times \dots \times A_n
\end{equation}
where $A_i = \regr{dom(v_i, a)}{o}$.

The last thing we need to update are the rewiring procedures.
When we want to allow conditional effects, factorized effect tasks in particular, they need to look as shown in Algorithm~\ref{lst:rew_inc} and Algorithm~\ref{lst:rew_self}, whereas \textsc{RewireOutgouing} stays the same as described by Seipp and Helmert (??).

\begin{algorithm}[t]
	\centering
	\input{./Listings/rewire_incoming_transition.tex}
	\caption{Rewiring of incoming transitions. The refinement in progress splits $[s]$ into states $d$ and $e$ on variable $v$. For the old transition $a \stackrel{o}{\rightarrow} [s]$ this procedure adds new transitions from $a$ to the new states $d$ and $e$ where necessary.}
	\label{lst:rew_inc}
\end{algorithm}

\begin{algorithm}[t]
	\centering
	\input{./Listings/rewire_self_loop.tex}
	\caption{Rewiring of self-loops. The refinement in progress splits state $[s]$ into states $d$ and $e$ on variable $v$. This procedure adds new self-loops and/or transitions between the new states $d$ and $e$ for the old self-loop $[s] \stackrel{o}{\rightarrow} [s]$ where necessary.}
	\label{lst:rew_self}
\end{algorithm}

Knowing that for the problem of the Rubik's Cube, no operator has a precondition, we can go even further and shorten Algorithm~\ref{lst:rew_self} to the one depicted in Algorithm~\ref{lst:rew_self_rc}.

\begin{algorithm}[t]
	\centering
	\input{./Listings/rewire_self_loop_without_preconditions.tex}
	\caption{Rewiring of self-loops. The refinement in progress splits state $[s]$ into states $d$ and $e$ on variable $v$. This procedure adds new self-loops and/or transitions between the new states $d$ and $e$ for the old self-loop $[s] \stackrel{o}{\rightarrow} [s]$ where necessary.}
	\label{lst:rew_self_rc}
\end{algorithm}

%\bibliographystyle{apalike}
%\bibliography{literature}

\end{document}