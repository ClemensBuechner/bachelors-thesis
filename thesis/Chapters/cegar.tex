\chapter{Factored Effect Tasks for Cartesian~Abstraction~Refinement}
\label{chp:factored}

The theory presented by \citet{seipp2018cegar} computes refinements of the abstractions in each iteration by applying regression.
However, the regression of a Cartesian state over general operators with conditional effects is not Cartesian.
Since \rubiks has only operators with conditional effects, we provide the theory for allowing conditional effects by introducing \emph{factored effect tasks}.

The first step is to understand why the theory proposed would not work for conditional effects.
The crucial point is to see that with conditional effects, the value of a variable in the state space is dependent on the state before applying the operator with conditional effects.
However, this is not the case when using operators without conditional effects: When applied, they will effect each variable occurring in their effects no matter which value they had before.
In their theory, \citet{seipp2018cegar} used the function $\mathit{post}(o)$ to denote the partial state over all variables occurring either in the precondition $\pre{o}$ or the effect $\eff{o}$ of an operator $o$.

For the problem of Rubik's Cube considered in this thesis, only one special case of conditional effects occurs: For all (conditional) effects triggered by an operator $o$ it is never necessary to check for other variables than the one that is changed by the effect.
We therefore introduce \emph{factored effect tasks} using \emph{factored effect operators}.

\begin{definition}
	\label{def:fact_opt}
	Operator $o$ is a \emph{factored effect operator} if $\eff{o}$ has the following form: $$\facteff{X}{x_1}{x_2} \wedge \facteff{X}{x_3}{x_4} \wedge \dots \wedge \facteff{Z}{z_1}{z_2}$$
	
	We write $\effects{o}$ for the set of effects $\facteffpair{X}{x_1}{x_2}$ where $x_1, x_2 \in dom(X)$, $x_1 \neq x_2$ and $\atom{X}{x_1}$ is the \emph{effect condition} and $\atom{X}{x_2}$ is the \emph{effect fact}.
	A factored effect operator cannot have two effects with equal effect conditions, \ie, $x_1 \neq x_3$ for all pairs of effects $\facteffpair{X}{x_1}{x_2}, \facteffpair{X}{x_3}{x_4} \in \effects{o}$.

	We say $X \in \vars{o}$ if either $X \in \vars{\pre{o}}$ or there is an effect $\facteffpair{X}{x_1}{x_2} \in \effects{o}$.
	
	Let further $\effects{o}[X] \subseteq \effects{o}$ be the set of fact pairs $\facteffpair{X}{x_1}{x_2}$ that are concerned with variable $X \in \mathcal{V}$.
	The set $\effects{o}[X]$ can be the empty set.
\end{definition}

\begin{definition}
	\label{def:fact_task}
	A \emph{factored effect task} is a planning task $\Pi = \langle \mathcal{V}, \mathcal{O}, s_0, s_\star\rangle$ where all $o \in \mathcal{O}$ are factored effect operators.
\end{definition}

Let us clarify the meaning of \cref{def:fact_opt,def:fact_task} with the following example:

\begin{example}
	\label{ex:count}
	Let $\Pi = \langle \mathcal{V}, \mathcal{O}, s_0, s_\star \rangle$ be the factored effect task of counting from zero to three with 
	\begin{itemize}
		\item $\mathcal{V} = \{c\}$ where $c$ is the counter variable with $dom(c) = \{0, 1, 2, 3\}$,
		\item $\mathcal{O} = \{\mathit{count}\}$ with $\effects{\mathit{count}} = \{\langle c=0, c=1 \rangle, \langle c=1, c=2 \rangle, \langle c=2, c=3 \rangle\}$ and $\pre{\mathit{count}} = \emptyset$, 
		\item $s_0 = \{\atom{c}{0}\}$ and $s_\star = \{\atom{c}{3}\}$
	\end{itemize}
\end{example}

In \cref{ex:count} it is impossible to tell the value of $c$ after applying $\mathit{count}$, when we do not have any information about the value of $c$ before applying $\mathit{count}$.
Therefore, it is impossible to take on the concept of a function $\mathit{post}(o)$.
However, we can find another way to achieve the same goal.

Considering a factored effect task $\Pi = \langle \mathcal{V}, \mathcal{O}, s_0, s_\star \rangle$, let $a$ be an abstraction of $\Pi$, let $o \in \mathcal{O}$ be a factored effect operator and let $X \in \mathcal{V}$ be a state variable.
Then the function $\resfact{\atom{X}{x_1}}{o}$ computes the value of $X$ after applying $o$ if $X$ has the value $x_1$ before applying $o$.
\begin{equation}
	\resfact{\atom{X}{x_1}}{o} = \left\{\begin{array}{ll}
		\atom{X}{x_2} & \text{if } \facteffpair{X}{x_1}{x_2} \in \effects{o} \\
		\atom{X}{x_1} & \text{otherwise}
	\end{array}\right.
\end{equation}

We define the function $\possible{a}{o}{X}$ to obtain a set of values that $X$ can possibly have after applying $o$ in any concrete state $s \in a$.
\begin{equation}
	\label{eq:possible}
	\possible{a}{o}{X} = \bigcup_{x \in dom(X, a)} \{\resfact{x}{o}\}
\end{equation}

We go on by working those revisions into the pseudo-code provided within the work of \citet{seipp2018cegar}.
\cref{lst:check} checks whether a transition exists between two abstract states via a given operator.
This corresponds to Algorithm~4 in the submission of \citet{seipp2018cegar}.

\begin{algorithm}
	\centering
	\input{./Listings/check_transition.tex}
	\caption{Transition check. Returns true iff factorized effect operator $o$ induces at least one transition between abstract states $a$ and $b$.}
	\label{lst:check}
\end{algorithm}

We also need to update the regression described by \citet{seipp2018cegar} in property \textbf{P4}.
In order to do that, we first need to define effect conditions.

\begin{definition}
	\label{def:effcond}
	Let $\atom{\ell}{dom(v)}$ be an atomic effect.
	The \emph{effect condition} $\effcond{\ell}{e}$ under which $\ell$ triggers given the effect $e$ is a propositional formula defined as follows:
	\begin{itemize}
		\item $\effcond{\ell}{\ell} = \top$
		\item $\effcond{\ell}{\ell'} = \bot$ for atomic effects $\ell' \neq \ell$
		\item $\effcond{\ell}{(e_1 \wedge \dots \wedge e_n)} = \effcond{\ell}{e_1} \vee \dots \vee \effcond{\ell}{e_n}$
		\item $\effcond{\ell}{(\mathcal{X} \vartriangleright e)} = \mathcal{X} \wedge \effcond{\ell}{e}$
	\end{itemize}
\end{definition}

The regression computes which value a variable $X$ can have before an operator $o$ has been applied, given the value assigned to $X$ at the moment.
Before applying $o$, $X$ can be mapped to either one of the following options:
\begin{itemize}
	\item The value of the effect condition of an effect with effect fact $\atom{X}{x_1}$, or
	\item $x_1$ itself, if there is no effect condition that triggers an effect $\atom{X}{x_2}$ where $x_1 \neq x_2$.
\end{itemize}

\begin{definition}
	\label{def:regr}
	Let $\atom{X}{x_1}$ be an atomic effect and let $o$ be an operator in a (general) planning task.
	Then the regression of $X$ through $o$ is defined as follows:
	\begin{eqnarray*}
		\regr{\atom{X}{x_1}}{\eff{o}} & = & \pre{o}[X] \wedge \left(\effcond{\atom{X}{x_1}}{\eff{o}} \vee \right. \\
		&& \left.\left(\atom{X}{x_1} \wedge \neg\effcond{X \neq x_1}{\eff{o}}\right)\right)
	\end{eqnarray*}
\end{definition}
	
Considering the special case of factored effect tasks, we can specialize the regression for the case of factored effect operators.
Let $o$ be such a factored effect operator, leading Equation~\ref{eq:disj} to show the regression of an atomic effect $\atom{X}{x_2}$ as described in \cref{def:regr}.

\begin{eqnarray}
	\label{eq:disj}
	\regr{\atom{X}{x_2}}{o} & = & \pre{o}[X] \wedge  \\
	&& \left(\bigvee_{\substack{\facteffpair{X}{x_1}{x_2} \in \\ \effects{o}}} \atom{X}{x_1} \vee \left(\atom{X}{x_2} \wedge \neg\bigvee_{\substack{\facteffpair{X}{x_3}{x_4} \in \\ \effects{o} : x_2 \neq x_4}}\atom{X}{x_3}\right)\right) \nonumber
\end{eqnarray}

We can rewrite Equation~\ref{eq:disj} by reasoning that either $\atom{X}{x_2}$ occurs as the effect condition of a factored effect or it does not.
This information is contained in the term $\neg\bigvee_{\facteffpair{X}{x_3}{x_4} \in\effects{o} : x_2 \neq x_4} \atom{X}{x_3}$:
For all factored effect pairs which do not have $\atom{X}{x_2}$ as their effect fact, we check whether their effect condition is false.
If so, we add $x_2$ to the possibilities of previous values for $X$ in order to end up with $\atom{X}{x_2}$ after applying $o$.
In other words, if $\atom{X}{x_2}$ occurs as the effect condition for any effect, this effect triggers for the case where $\atom{X}{x_2}$ held before applying $o$ and therefore cannot hold anymore afterwards.
This contradicts our knowledge since we are regressing over the fact $\atom{X}{x_2}$ and therefore $x_2$ is not a valid value for $X$ before applying $o$.
This leads to the case distinction shown in Equation~\ref{eq:case}.

\begin{eqnarray}
	\label{eq:case}
	\regr{\atom{X}{x_2}}{o} & = & \pre{o}[X] \wedge \\
	&& \left(\bigvee\limits_{\substack{\facteffpair{X}{x_1}{x_2} \in \\ \effects{o}}} \atom{X}{x_1} \vee \left\{\begin{array}{ll}
		\atom{X}{x_2} & \begin{array}{l}
			\text{if } \atom{X}{x_2} \text{ does not} \\ \text{occur as an effect} \\ \text{condition in } o 
		\end{array} \\
		\bot & \text{otherwise}
	\end{array}\right.\right)\nonumber
\end{eqnarray}

This definition is illustrated by \cref{ex:regr}.

\begin{example}
	\label{ex:regr}
	Let $o$ be an operator of a planning task with no precondition ($\pre{o} = \top$) and the following effect: $\eff{o} = \facteff{X}{x_1}{x_2} \wedge \facteff{Z}{z_1}{z_2} \wedge \facteff{Z}{z_2}{z_1}$.
	Then
	\begin{eqnarray*}
		\regr{\atom{Z}{z_1}}{o} & = & \top \wedge \regr{\atom{Z}{z_1}}{\eff{o}} \\
		& = & (\atom{Z}{z_2} \vee (\atom{Z}{z_1} \wedge \neg(\atom{Z}{z_1}))) \\
		& = & \atom{Z}{z_2}
	\end{eqnarray*}
\end{example}

Since we want to compute the regression not only for one atomic effect but for a whole abstract state, we need to add the following to our definition:

For a set $\mathcal{X} \subseteq dom(X)$ of values for a variable $X$ we define the regression as
\begin{equation}
	\regr{\mathcal{X}}{o} = \bigcup_{x \in \mathcal{X}} \regr{\atom{X}{x}}{o}
\end{equation}
and for an abstract state $a$ as
\begin{equation}
	\regr{a}{o} = A_1 \times \dots \times A_n
\end{equation}
where $A_i = \regr{dom(v_i, a)}{o}$.
These changes suffice to make CEGAR applicable to factored effect tasks.
