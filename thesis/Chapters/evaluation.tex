\chapter{Evaluation}
\label{chp:eval}

In this chapter we present our evaluations of the abstraction heuristics introduced in \cref{sec:abstractions}.
Before doing so, we describe the setting of our experiments.

%% ----------------------------------------------------------------
\section{Experiment Setup}
\label{sec:setup}
In order to evaluate abstraction heuristics for \rubiks we first needed to generate a set of problem files.
We did so by writing a Python script which takes input parameters like the size, a parameter to choose which operators should be allowed, and the number of turns for scrambling \rubiks.
When allowing all 18 operators, this last parameter gives an upper bound on the number of turns needed to solve the problem instance that is generated, since we could simply reverse the order of the scrambling moves and switch the turn direction to obtain a plan to solve the task.
In some instances of the scrambling process, moves cancel each other out, thus the actual optimal plan could be shorter than the number of turns passed to the script.

Using the script described above, which we called 10 times for each value from 1 through 20 for the turns parameter, we then generated a set of 200 problem instances for the \rubiks.
In the process, we made sure to avoid duplicates by storing hashes of the initial states and redoing the generation step for detected duplicates in the hash map.

We used Downward Lab \cite{seipp2017lab} in order to facilitate to set up the evaluation of our problems.
Furthermore, we ran our experiments on the sciCORE high-performance computing infrastructure.
The following paragraphs provide more information about the heuristics used for our final experiments.
The search was done with the commonly used and well known \astar search algorithm~\citep{hart1968formal}.

\paragraph{Blind Search Heuristic}
As a base line we used the blind search heuristic.
We expected it to only work on the problems that have short solutions, but we use the number of states expanded in comparison to the other heuristics.
The heuristic is part of Fast Downward~\cite{helmert2006fast}.

\paragraph{Maximum over Manual Patterns}
We divided the 20 variables into five patterns of equal size.
Following \citet{korf1997finding}, each pattern consists either only of \corners or only of \edges.
Since we were using an implementation of pattern databases that is not optimized for \rubiks, we were not able to make our patterns as large as the ones \citeauthor{korf1997finding} used.
While he kept all \corners in one pattern and divided the \edges only into two patterns, we had two patterns for \corners and three for \edges.
This makes four cubies per pattern, which was the limit that could be handled by the implementation that was already provided in Fast Downward~\cite{helmert2006fast}.
Our patterns are the following:
\begin{itemize}
	\item \corners of the F$_0$-layer,
	\item \corners of the F$_2$-layer,
	\item \edges of the F$_0$-layer,
	\item \edges of the F$_1$-layer, and
	\item \edges of the F$_2$-layer.
\end{itemize}
After looking up the abstract goal distance for each pattern independently, we took the maximum over these values for each pattern to get a lower bound on the number of moves necessary to get to the goal.

\paragraph{Maximum over Systematic Patterns}
The strategy applied here is the same as for the fixed patterns: We take the maximum value over the values computed for a set of smaller patterns.
The difference, however, lies in the choice of these patterns.
While before, we were selecting patterns manually, we now use all \emph{interesting} patterns up to a given size~\cite{pommerening2013getting}.
This method too was already implemented in Fast Downward \cite{helmert2006fast}.
The only parameter needed by the algorithm is the maximum size of each pattern.
We ran a separate experiment that tested different values from 2 up to 6 in order to find this value.
The most promising one was found to be 3 which was then adopted into our final experiment.

\paragraph{Single Pattern of Corner Cubies or Edge Cubies}
We also wanted to find out how informative a single pattern of four variables is.
Assuming to have symmetries in the patterns of \corners and also in the patterns of \edges, we include the pattern for the \corners of layer F$_0$ and the pattern for the \edges of the same layer.

\paragraph{CEGAR}
As already discussed in \cref{chp:factored}, the implementation provided by~\citet{seipp2018cegar} was not applicable for tasks that have operators with conditional effects.
However, their implementation was taken as a basis for our evaluations.
Therefore, we had to change the code in the same places as the theory.
We then did some experiments on varying parameters for the search with CEGAR.
Concretely, we tested which configuration of limiting the number of transitions or the time yields the best result.
In the end, we chose to limit time by a maximum of 900 seconds and allowing infinitely many transitions for the final experiments.

\paragraph{\Merge}
For the \merge heuristic it was possible to take the already implemented version which can also handle conditional effects.
It uses the currently recommended \merge configuration that employs the DFP-SCC merge strategy, bisimulation and at most 50'000 states.

%% ----------------------------------------------------------------
\section{Results}
For the results presented and discussed in this section we refer to the tables gathered in \cref{appendix:results}, which show a portion of the reports generated with Downward Lab \cite{seipp2017lab}.
They contain the initial $h$-values computed or rather the number of expanded states until the last $f$-layer for all of our 200 problem instances and all heuristics evaluated.

\subsection{Coverage and Errors}
\cref{tab:summary} provides an overview of the number of solved problems with each configuration and also the reason of failure regarding the unsolved problems.
The coverage denotes the amount of problems that could be solved by either configuration.
The sum over coverage, out-of-memory and timeout sums up to 200 for each configuration.
The last entry denoted as total time is the calculated geometric mean of time used to find a solution.
Problems where no solutions were found are not considered in this value.

\begin{table}[h]
	\centering
	\input{./Tables/summary}
	\caption{Values summed up over all 200 instances and the geometric mean for the time needed to find a solution.}
	\label{tab:summary}
\end{table}

Within the limits of our working environment, all heuristic search algorithms find more solutions than blind search.
The highest coverage is found by applying projection using pattern databases with our manual patterns, followed by systematic patterns.
The configuration using systematic pattern generation needs significantly more time for finding paths.
It is also the only configuration that ever runs out of time before memory.
In fact, it never runs out of memory.
Since an initial $h$-value is found with systematic PDBs for all problem instances, we can rule out that the process of generating the patterns takes too long.
We deduce that this heuristic is slow to evaluate whereas all other heuristics are fast to evaluate.
The systematic pattern size of three finds 1'350 \emph{interesting} patterns for \rubiks, which are actually all patterns of size three or smaller.
Thus, we assume that the difference in time needed for evaluating is dominated by the number of patterns.
Still, it performs better than only using one single pattern to estimate the goal distance.

Only using approximately double the time of blind search, our CEGAR implementation is in third place considering coverage.
Meanwhile, the memory overhead of merge-and-shrink seems to be rather high, given that it only finds solutions for less than 50\% of the problems, while our manual patterns cover almost 65\% of the problems.
We find nine instances with a solution cost 12, which is the highest solution cost among all instances.
This leads to the conclusion that we are not able to solve problems of \rubiks with an optimal plan cost of 13 or more using any of our configurations.
This means that with our configurations we can only solve 0.0001\% of all 43'252'003'274'489'856'000 reachable states in the state space.
This is according to the list\footnote{available online at \url{http://cube20.org} (accessed: May 29, 2018)} that emerged as a byproduct of the proof that all configurations of \classic{s} can be solved in 20 steps \cite{rokicki2014diameter}.

The plan costs found in our experiments are identical throughout either configuration for each problem.
They are never higher than the number of turns to initialize the problem.

\subsection{Initial $h$-value}
In order to compare the configurations by their informativeness, we use the $h$-values found for the initial states of our problem instances.
The higher the value is, the more informed we assume the heuristic to be.
In \cref{tab:hval} we see that CEGAR is the undisputed leader in this category.
It shows the highest value over all configurations for every problem instance.
Furthermore, it agrees with the costs for the found plans up to 9 initial turns.
From then on, the initial $h$-value varies somewhere in the range of 8 and 9, but interestingly never goes up to 10 or higher.

The second best informed heuristic among the evaluated is one of the projections.
Both manual patterns as well as systematic patterns perform somewhere on the same level where they mostly compute the same value and only rarely vary by more than 1.
It is not clear which one is more informative since none of them is always better or worse than the other.
They never compute an initial $h$-value of more than 7.
The projection heuristics that only consider one pattern do even worse, where again it is not clear whether one of them outperforms the other.
For example, for problem p\_2 of 4 turns we can see that they can vary a lot, even though both are concerned with the cubies belonging to the front face.

Surprisingly, \merge performed rather poorly.
Even though being the most general class of abstractions among the considered, it has the lowest coverage and does not convince with the found initial $h$-values.
At least for \rubiks, it seems that our implementation does not fulfill the expectations of higher potential over other classes of abstractions.
While the initial $h$-value is perfect for problem instances with 1 through 3 turns, the initial $h$-value only varies between 3 and 4 for all other instances.
Therefore, assuming that most states in the state space will have a value of 3 or 4, we suppose that not much change in the $h$-value can be expected from one state to another.
We can only assume that this behavior originates where the abstraction is built, even though there is no evidence of running out of time or anything alike.

\subsection{Expansions Until Last $f$-layer}
In this section, we set the heuristics in relation to one another.
In order to do so, we compare the strategies in pairs, where the pairs are neighbors in the order of generality.
We use the number of expansions until the last $f$-layer and we display the values in scatter plots.
This number denotes how many expansions are necessary to get to the point where the next expansion could reach a goal state.

\cref{fig:ms_vs_cegar} shows that our implementation of CEGAR outperforms the \merge heuristic.
Most of the data points are situated on the bottom line of the scale, which means that CEGAR barely has to do any expansions before reaching the goal.
Only very few values approach the diagonal, which indicates the threshold on whether a data point speaks for one or the other implementation.

\begin{figure}
	\centering
	\input{./Figures/ms-vs-cegar}
	\caption{Expansions until the last $f$-layer for \merge vs.\ CEGAR on a loglog scale.}
	\label{fig:ms_vs_cegar}	
\end{figure}

Next up in the ordering are the projections.
We compare CEGAR to the fixed patterns in \cref{fig:cegar_vs_pdb}.
While at first CEGAR again has a set of values very close to zero, at some point its numbers of expansions until the last $f$-layer increase rapidly and the data points fall below the boundary set by the diagonal.
The behavior is similar for either manual patterns as well as for systematic pattern generation.
We interpret that for problem instances that are further away from the goal, the heuristics using pattern databases both outperform CEGAR.

\begin{figure}
	\centering
	\hspace{1.8cm}
	\input{./Figures/cegar-vs-pdb}
	\caption{Expansions until the last $f$-layer for CEGAR vs.\ PDB heuristics on a loglog scale.}
	\label{fig:cegar_vs_pdb}	
\end{figure}

Also, when comparing the singleton PDB's for the \corners, or rather \edges, on the front face, we recognize high correlation.
In \cref{fig:corners_vs_edges} we see that neither can get ahead of its counterpart.

\begin{figure}
	\centering
	\input{./Figures/corners-vs-edges}
	\caption{Expansions until the last $f$-layer for one pattern of \corners vs.\ one pattern for \edges on a loglog scale.}
	\label{fig:corners_vs_edges}	
\end{figure}

When using CEGAR, we find a lot of problem instances for which we do not have to do any expansions until the last $f$-layer, which denotes the heuristic to be perfect for these instances.
However, as soon as we get to more complex instances, the number goes up and CEGAR is outperformed by PDBs, which have also shown higher coverage.