\chapter{Classical Planning}
\label{chp:planning}

\rubiks as it is described in \cref{chp:back_rubiks} is a \emph{classical planning task}.
The goal of classical planning is to find a path from one state to another in a \emph{state space} by applying multiple actions~\citep{hoffmann2011everything}.
Since such state spaces are mostly by far too large to search paths without any further information, we make use of heuristics that help us search towards promising directions.

%% ----------------------------------------------------------------
\section{Planning Tasks and State Spaces}
A planning task is defined as an assignment of finding a path from an initial state to a goal state.
The following definition in finite-domain representation is taken from \citet{helmert2017planning}.

\begin{definition}
	A \emph{planning task} is a 4-tuple $\Pi = \langle \mathcal{V}, I, \mathcal{O}, \gamma\rangle$ where
	\begin{itemize}
		\item $\mathcal{V}$ is a finite set of \emph{finite-domain state variables},
		\item $I$ is a state over $\mathcal{V}$ called the \emph{initial state},
		\item $\mathcal{O}$ is a finite set of \emph{finite-domain operators} over $\mathcal{V}$, and
		\item $\gamma$ is a formula over $\mathcal{V}$ called the \emph{goal}.
	\end{itemize}
\end{definition}
The following additions to this definition are adopted from \citet{seipp2018cegar}:
\begin{itemize}
	\item The mapping $\atom{V}{v \in \dom{V}}$ of a variable $V \in \mathcal{V}$ is called an \emph{atom}.
	We denote a formula over $\mathcal{V}$ as a \emph{partial state}.
	Atoms are used to assign values in their respective domains to a subset of variables $\vars(s) \subseteq \mathcal{V}$ in partial state $s$.
	We write $s\left[V\right] \in \dom{V}$ for the value which $s$ assigns to the variable $V$.
	Partial states defined on all variables are called \emph{states}, and $S(\Pi)$ is the set of all states of $\Pi$.
	\item Each operator $o \in \mathcal{O}$ has a \emph{precondition} $\pre{o}$, an \emph{effect} $\eff{o}$ and a non-negative \emph{cost} $\cost{o} \in \mathbb{R}_0^+$.
	The precondition $\pre{o}$ and effect $\eff{o}$ are partial states.
	An operator $o \in \mathcal{O}$ is applicable in state $s$ if $\pre{o} \subseteq s$.
\end{itemize}

Each planning task induces a state space which we define according to \citet{helmert2018ai}.

\begin{definition}
	\label{def:state-space}
	A \emph{state space} is a 6-tuple $\mathcal{S} = \langle S, A, \cost, T, s_0, S_\star\rangle$ with
	\begin{itemize}
		\item $S$ a finite set of \emph{states},
		\item $A$ a finite set of \emph{actions},
		\item $\cost : A \rightarrow \mathbb{R}_0^+$ the \emph{action costs},
		\item $T \subseteq S \times A \times S$ the \emph{transition relation} from one state to another through an action,
		\item $s_0 \in S$ the \emph{initial state}, and
		\item $S_\star \subseteq S$ the set of concrete \emph{goal states}.
	\end{itemize}
	
	A \emph{plan} is a path from $s_0$ to $s_\star \in S_\star$ labeled by the actions of each transition taken to get to $s_\star$.
	The plan is called \emph{optimal} if the sum of costs along the path is minimal.
\end{definition}

We further define effects according to \citet{helmert2017planning}.

\begin{definition}
	\label{def:effect}
	\emph{Effects} over state variables $\mathcal{V}$ are inductively defined as follows:
	\begin{itemize}
		\item If $V \in \mathcal{V}$ is a state variable, then $\atom{V}{d \in \dom{V}}$ is an effect \emph{(atomic effect)}.
		
		\item If $e_1, \dots, e_n$ are effects, then $(e_1 \wedge \dots \wedge e_n)$ is an effect \emph{(conjunctive effect)}.		
		The special case with $n = 0$ is the \emph{empty effect $\top$}.
		
		\item If $\mathcal{X}$ is a logical formula and $e$ is an effect, then $(\condeff{\mathcal{X}}{e})$ is an effect \emph{(conditional effect)}.
	\end{itemize}
\end{definition}

%% ----------------------------------------------------------------
\section{Abstraction Heuristics}
\label{sec:abstractions}

The solution strategies evaluated within this thesis are connected since they abstract the considered state space in order to simplify a planning task.
Abstraction usually happens when a state space is modified into a coarser version by losing some distinctions~\citep{seipp2018cegar}.
Abstract state spaces have fewer states and lead to a more tractable analysis~\citep{helmert2014merge}.
According to \citeauthor{helmert2014merge}, every abstraction yields an admissible heuristic, since abstractions preserve paths in the underlying transition graph~\citep{helmert2007flexible}.
The following sections present the methods we apply in our evaluations.

\subsection{Projections}
\label{subsec:pdbs}
Instead of solving a comparatively large problem, we can use projections to shrink the effort.
By only focusing on parts of the underlying problem, projections disregard some of the available information on purpose.
We use pattern databases and only take into account a subset of all state variables.
The method can be seen as an adaption of human problem solving behavior for combinatorial puzzles or multi-player games where we often apply a \quotate{divide-and-conquer} strategy to reach intermediate goals~\citep{culberson1998pattern}.

We use the term of \emph{target patterns} similar to \citeauthor{culberson1998pattern}: One target pattern describes a partial specification of the goal.
Then the \emph{pattern database} (PDB) is the set of all patterns that can be obtained by permutations over the variables in a target pattern.
We compute the cost of each pattern in the PDB, which corresponds to the minimal distance to its target pattern.
These values are stored in a lookup table.
This gives a lower bound to the complete planning task since we cannot find a solution without solving the sub-problem for the considered target pattern~\citep{korf1997finding}.

We can either look up the value found for one pattern or combine the values of multiple patterns in order to come up with a heuristic value for a state.
For example, taking the maximum value over two or more patterns will always be at least as good as looking only at one of these values.
Since the pattern that provides the maximum value is still a lower bound on satisfying its target pattern, this value is the minimum cost we must expect for solving the overall problem.

\subsection{\Cartesian{s}}
\label{subsec:cegar}
The idea behind \emph{\cartesian}, which is a rather new class of abstractions, is to start from a coarse and maybe inaccurate abstraction and iteratively improve it~\citep{seipp2018cegar}.
The refinements in each iteration happen only where flaws in the previous abstraction were detected.
The method introduced by \citeauthor{seipp2018cegar} is called counterexample-guided abstraction refinement (CEGAR).
Its goal is to derive \cartesian heuristics for optimal classical planning.
It is a variation of the work published by \citet{clarke2000counterexample} who have applied this technique for model checking.

The concept is more general than PDBs.
In comparison, \cartesian does not choose patterns of variables present in a planning task but takes apart the corresponding domains of the variables.
We define \emph{Cartesian sets} and \cartesian according to \citet{seipp2018cegar} in \cref{def:cartesian}.

\begin{definition}
	\label{def:cartesian}
	A set of states for a planning task with variables $\langle v_1, \dots, v_n \rangle$ is called \emph{Cartesian} if it is of the form $A_1 \times A_2 \times \dots \times A_n$, where $A_i \subseteq dom(v_i)$ for all $1 \leq i \leq n$.
	
	An abstraction is called \emph{Cartesian} if all its abstract states are Cartesian sets.
	
	For an abstract state $a = A_1 \times \dots \times A_n$, we define $dom(v_i, a) = A_i \subseteq dom(v_i)$ for all $1 \leq i \leq n$ as the set of values that variable $v_i$ can have in abstract state $a$.
\end{definition}

An abstraction of a state space is called Cartesian if all its abstract states are Cartesian sets~\citep{seipp2018cegar}.
Given a planning task with variables $\langle V_1, \dots, V_n \rangle$, a set of states is Cartesian if it is of the form $A_1 \times A_2 \times \dots \times A_n$ where $A_i \subseteq \dom{V_i}$ for all $1 \leq i \leq n$.
In other words, abstract states are collections of concrete states with certain similarities.
A transition between two abstract states is present if a transition between two concrete states, one from either abstract state, exists.

Another difference to pattern databases is the variance in granularity of the different abstract states.
In \cartesian, abstract states can either consist of a single concrete state or of a large number of concrete states~\citep{seipp2018cegar}.

Instead of searching for one abstraction of the state space, \citeauthor{seipp2018cegar} suggest using additive abstractions that can also be used to estimate a heuristic value when combined together.
In order to combine Cartesian abstractions admissibly, they further introduce saturated cost partitioning.
Since this part diverges from the aims of this thesis, we will not go any further into detail.
However, their work also provides an overview over other abstraction methods present in our evaluations.

\subsection{Merge-and-Shrink Abstractions}
\label{subsec:m-and-s}
The idea behind merge-and-shrink abstractions originated in model checking for automata networks~\citep{helmert2014merge}.
The idea was carried further to be used as a heuristic function and was later applied by \citeauthor{helmert2014merge} to planning as a new class of abstracting planning tasks.

Building merge-and-shrink abstractions is an iterative procedure and consists of the merge step and the shrink step.
Starting from atomic projections, abstractions are merged together and therefore grow during the merge step, whereas in the shrink step, others are shrunk and apply additional abstractions.

Merge-and-shrink is the most general among the considered classes of abstractions.
In other words, instances of the less general abstractions can also be the result when applying \merge.
So, \merge can derive the same abstractions as \cartesian and projections, but can also find abstractions that are considered to be neither Cartesian nor projections.
We therefore expect \merge to perform better in the evaluations.