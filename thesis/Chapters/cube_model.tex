\chapter{Modeling \rubiks}
\label{chp:rubiks_model}

Even though \rubiks exists in a range of various forms and sizes, we decided to restrict our evaluations to use only the classical \cube{3} as described in \cref{chp:back_rubiks}.
This chapter introduces the chosen formalization for the problem.

%% ----------------------------------------------------------------
\section{Choice of State Space}
\label{sec:state_choice}
When choosing a state space representation of a problem, we always want to keep it as small as possible in order to find solutions faster.
The smaller the state space is, the less states have to be considered within the search.
For \rubiks, we have examined two different approaches.

\subsection{Model using Facelets}
The state of \rubiks can be described by its facelets.
There are 24 facelets on \corners and 24 facelets on \edges.
Also, there are 24 locations where a corner-facelet can possibly be placed, as well as 24 locations for edge-facelets.
This results in a total amount of $24^{24} \cdot 24^{24} = 24^{48}$ possible permutations of all facelets.

For each operator we find the following amount of effects:
Having 12 facelets on the \corners of one face and 24 facelets on \corners in total, one operator will yield $12 \cdot 24 = 288$ effects.
Additionally, with 8 facelets on the \edges of one face and totally 24 facelets on \edges, we have another $8 \cdot 24 = 192$ effects .
This sums up to a total of 480 effects per operator.

Because we ignore the fact that all facelets on the same cubie are stuck together and cannot be moved around the cube independently, this model allows a lot of unreachable states.
The resulting numbers of states and effects are thus suboptimal.

\subsection{Model using Cubies}
We can improve this representation by focusing on cubies instead of facelets.
Given 8 \corners with 3 possible rotations and 12 \edges with 2 possible rotations and only 8 locations for \corners and 12 locations for \edges, we get a reduction of the state space.
Thus, the number of permutations within our state space reduces to $(8 \cdot 3)^8 \cdot (12 \cdot 2)^{12} = 24^8 \cdot 24^{12} = 24^{20}$ states.

The number of effects per operator is also reduced:
For any action, we have 4 \corners on the according face, each with three rotations.
Therefore, there are $4 \cdot 3 \cdot 8 = 96$ effects for \corners.
Accordingly, we have 4 \edges on a face with 2 rotations each, which yields $4 \cdot 2 \cdot 12 = 96$ effects as well.
Summing up these yields a total of 192 effects per operator.

We could go even further and remove one more cubie per category from the model.
This is because its position and rotation can be determined by the positions and rotations of all other cubies of the same category \citep{korf1997finding}.
However, this is not done within the scope of this thesis in order to keep the generation of problem files a little simpler and more intuitive.

%% ----------------------------------------------------------------
\section{Coordinate System}
We establish an ordering of cubies on \rubiks by enumerating them starting at the top left corner of the front face with a value of 0.
Each category is considered for itself, since cubies cannot move between locations of different categories.
From top to bottom, left to right, front to back, we assign the next higher integer number to each cubie of the given category.
\cref{fig:coordinates} illustrates the values of the visible cubies where the categories are distinguished by the color of the labels: \Corners are displayed in red whereas \edges are marked in blue.

\begin{figure}
	\centering
	\input{./Figures/rubiks_cube_coordinates}
	\caption{The coordinate system used for our model of a \classic.}
	\label{fig:coordinates}
\end{figure}

%% ----------------------------------------------------------------
\section{Locations and Rotations}
\label{sec:loc_and_rot}
Each cubie considered for \classic{s} has two values which define its state unambiguously.
Firstly, we need to specify where on \rubiks each cubie is located in a state.
For example, a \corner can be located on either of the eight \corner locations.
Secondly, it can lie in one of three different rotation directions.
\Edges, however, can be located on either of the twelve \edge locations and their rotation can be either of two directions.

We take the coordinate as the value for denoting its location on the cube.
Even though we have the same values for coordinates of \corners as for \edges, this is still unambiguous since we know whether a cubie is a \corner or an \edge.

For describing the rotation value, we imagine a three-dimensional object as shown in \cref{fig:rotation}.
We describe its current rotation by a triple $\langle x,y,z\rangle$ where $x$ corresponds to the color of the plane perpendicular to the $x$-axis, namely the $y$-$z$-plane.
Accordingly $y$ is the color of the $x$-$z$-plane and $z$ the color of the $x$-$y$-plane.
The example shown in \cref{fig:rotation} is denoted as $\langle r,g,b\rangle$, where the starting letter of the colors are taken as the values in the triple.

As in the \rubiks-problem, we only allow rotations by \degs{90} around either of the $x$-, $y$- or $z$-axis.
Applying a rotation around the $x$-axis switches the values of $y$ and $z$ in the triple and in our example results in the triple $\langle r,b,g\rangle$ which corresponds to \cref{fig:rotation_x}.
Similarly, turning around the $y$-axis will switch the values of $x$ and $z$ and turning around the $z$-axis switches $x$ with $y$ accordingly.
The resulting rotation triple is not dependent on the direction of the turn and turning by \degs{180} does not have any effects.

\begin{figure}
	\centering
	\begin{minipage}{.45\textwidth}
		\centering
		\input{./Figures/rotations}
		\caption{Rotation imagination aid.}
		\label{fig:rotation}
	\end{minipage}
	\hfill
	\begin{minipage}{.45\textwidth}
		\centering
		\input{./Figures/rotations_x}
		\caption{Rotation after a turn of \degs{90} around the $x$-axis.}
		\label{fig:rotation_x}
	\end{minipage}
\end{figure}

For \edges we use the same notation, but denote one plane's color as \# (speaking \quotate{blank}), which corresponds to the direction where no facelet is visible from the outside of the cube.
Then, rotations on the rotation triple can be performed similar to the case of \corners.

We have stated before that we only need three rotation values to manifest a cubie's state together with its location.
The reason for this is that every \corner has three visible facelets which cannot be interchanged and each facelet can be turned towards either of three directions in one position.
However, there are six different rotation triples that can be achieved by applying rotations.
In order to reduce the representation as a rotation triple to only three values (or rather two for \edges) we apply the following:
From a rotation triple $\langle x,y,z\rangle$ we only need information about the value in one direction and then together with the position of the cubie can conclude the other two.
Therefore, the first non-blank element of the triple is taken to be the rotation value, which has three possibilities in the case of \corners and two for \edges.

%% ----------------------------------------------------------------
\section{Formalization}
We formalize planning tasks considered within this thesis in the commonly used \sas formalism.
As we have established in \cref{sec:state_choice}, we have a rather compact description of the state space when using cubies.

\subsection{Separating Locations from Rotations}
\label{subsec:separate}
Our first version of the model uses 40 variables, which makes two per cubie.
One of these is concerned with the location, the other denotes the rotation.
The domain of the location variable for \corners therefore is a value from 0 through 7, whereas for \edges it is a value from 0 through 11.
The domain of the rotation variable for \corners is a value from 0 through 2, whereas for \edges it is either 0 or 1.

If one \corner is at any location, no other \corner can be located there as well.
This knowledge can be modeled with eight mutexes for \corners and twelve for \edges.
There are even more restrictions that could be captured with mutexes in the \sas formalism.
For example, it is not possible to mutate a single cubie, meaning that we cannot apply a series of actions that returns to its starting state and only changes the rotation value of one cubie.
Even interchanging two cubies is not possible, so literature talks about \emph{3-cycles} that can be performed on \rubiks \citep{joyner2008adventures}.
However, we did not model these in our description of the state space.

Operators for \rubiks do not have preconditions, so they are all applicable in every state.
However, the effects of each operator only happen under certain circumstances.
We call these the effect conditions and all effects of operators have at least one such condition.
For example, a cubie will only be affected by the F operator if it is located on the front face before F is applied.
Consequently, it will also be on the front face after applying F since all cubies not on F keep their location.
Effects for location variables, on one hand, have exactly this effect condition.
On the other hand, effects for the rotation variable of the same cubie need information about its location as well as its rotation value in order to tell whether or not the effect triggers.
\cref{ex:model_sep} illustrates the explanations above.
\begin{example}
	\label{ex:model_sep}	
	Let $c_i$ be the variable for the cubie that is located at coordinate $i$ in the solved \rubiks.
	Let $r_i$ be the rotation of the same cubie.
	The atom $\atom{c_i}{j}$ maps $c_i$ to coordinate $j$.
	Accordingly, $\atom{r_i}{j}$ maps $r_i$ to one of the possible rotation values.
	Then applying operator U to turn the upper face by \degs{90} has the following effects $\condeff{a}{b}$ where $a$ is the effect condition and $b$ is the effect fact:
	
	\begin{eqnarray*}
		\atom{c_0}{0} & \vartriangleright & \atom{c_0}{4} \\
		\atom{c_0}{0} \wedge \atom{r_0}{0} & \vartriangleright & \atom{r_0}{1} \\
		\atom{c_0}{0} \wedge \atom{r_0}{1} & \vartriangleright & \atom{r_0}{2} \\
		\atom{c_0}{0} \wedge \atom{r_0}{2} & \vartriangleright & \atom{r_0}{0} \\
		\atom{c_0}{2} & \vartriangleright & \atom{c_2}{0} \\
		& \dots \\
		\atom{c_7}{6} \wedge \atom{r_7}{2} & \vartriangleright & \atom{r_7}{0} \\
		\atom{c_8}{1} & \vartriangleright & \atom{c_9}{4} \\
		& \dots \\
		\atom{c_{19}}{9} \wedge \atom {r_{19}}{1} & \vartriangleright & \atom{r_{19}}{1}
	\end{eqnarray*}
\end{example}

\subsection{Combining Locations and Rotations}
\label{subsec:combining}
The model described above does not meet all requirements needed for purposes explained in \cref{chp:factored}.
We want our model to have effects with only one effect condition and furthermore, the condition can only depend on the variable that is changed by the effect fact.
We can keep our cube model as it is, but we have to apply some minor changes to its formalization.

The \sas-representation we established combines the rotation variable with the location variable.
This leads to a total number of only 20 variables, which means one variable per cubie.
The domain of each variable corresponds to the cross-product of the possible locations and rotations of the according cubie.
To calculate the value for any cubie, we multiply the location value by the number of possible rotations and add the rotation value.
For example, a \corner at coordinate 5 with rotation 2 is assigned the value $5 \cdot 3 + 2 = 17$.
The other way around, if $v$ is the value of a cubie in our adjusted state space and $r$ is the number of possible rotations for the cubie, we get the location of a cubie by calculating $\left\lfloor\frac{v}{r}\right\rfloor$ and its rotation value by $v\mod r$.
\cref{ex:model_comb} shows the same effects as \cref{ex:model_sep} but for the newly introduced model.
\begin{example}
	\label{ex:model_comb}
	Let $c_i$ be the variable for the cubie that is located at coordinate $i$ in the solved \rubiks.
	Let $r_i$ be the number of possible rotations for the same cubie.
	The atom $\atom{c_i}{j}$ maps $c_i$ to coordinate $\left\lfloor\frac{j}{r_i}\right\rfloor$ and the rotation value $j \mod r_i$.
	Then applying operator U to turn the upper face by \degs{90} has the following effects $\condeff{a}{b}$ where $a$ is the effect condition and $b$ is the effect fact:
	\begin{eqnarray*}
		\atom{c_0}{0} & \vartriangleright & \atom{c_0}{13} \\
		\atom{c_0}{1} & \vartriangleright & \atom{c_0}{14} \\
		\atom{c_0}{2} & \vartriangleright & \atom{c_0}{12} \\
		\atom{c_0}{6} & \vartriangleright & \atom{c_0}{5} \\
		& \dots \\
		\atom{c_7}{20} & \vartriangleright & \atom{c_7}{6} \\
		\atom{c_8}{2} & \vartriangleright & \atom{c_8}{8} \\
		& \dots \\
		\atom{c_{19}}{19} & \vartriangleright & \atom{c_{19}}{13}
	\end{eqnarray*}
\end{example}
