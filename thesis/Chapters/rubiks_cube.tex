\chapter{\rubiks}
\label{chp:back_rubiks}

Originally designed for students to understand three-dimensional problems, Prof.~Ern\~o~Rubik invented \rubiks, also known as \quotate{Magic Cube}, in the year~1974~\citep{rubiks}.
While his prototype had a size of $4 \times 4 \times 4$, we will generally refer to the \emph{\classic} as a \cube{3}, since nowadays this is the most commonly used model.
Meanwhile, a wide range of forms and variations have been introduced under the denomination of \rubiks, but we restrict ourselves to the classical one which is a cube with six faces of equal magnitude which are perpendicular to one another, as it is depicted in \cref{fig:rubiks}.

\begin{figure}
	\centering
	\input{./Figures/plain_rubiks_cube}
	\caption{Solved \classic of size 3.}
	\label{fig:rubiks}
\end{figure}

%% ----------------------------------------------------------------
\section{Terminology}
We introduce terms and expressions to simplify the wording for \rubiks and its states.
Certain terms introduced in this section correspond to the wording of a cube as a mathematical body.
Please note that we only consider three-dimensional (Rubik's) cubes within this thesis.

\paragraph{Cubie}
\rubiks{s} are \quotate{cut} uniformly into multiple slices parallel to each face, resulting in a large amount of smaller cubes which we call \emph{cubies}.
On \classic{s} we find three different kinds of cubies.
The term \emph{category} when referring to all cubies of the same kind.
The cubie in the inside of a cube can never appear on the surface as a result of any combination of actions as described in \cref{subsec:operators}.

\paragraph{Face}
A set of cubies covering one whole side of \rubiks is called \emph{face}.
In \cref{fig:rubiks_faces}, the cubies marked grey are those of the face turned towards the viewer.
To denote actions later on, we assign names to the faces.
The face highlighted in gray in \cref{fig:rubiks_faces} is called the front face (F), in the opposite direction we have the back face (B).
Furthermore, we have the left face (L), the right face (R), the upper face (U) and the downward face (D).
We call the faces of a cubie \emph{facelet} in order to avoid confusion with the term of faces on the cube as a whole.

\paragraph{Corner}
The \emph{corners} of \rubiks are those cubies positioned on three faces at once.
There are eight \corners on a \classic.
There are three facelets on the surface on each corner cubie.
\cref{fig:rubiks_corners} highlights the visible \corners in gray.
The \corner that is located on the downward, left and back face is not visible at all.

\paragraph{Edge}
Adapting the concept from mathematics, edges are the cubies located between two \corners.
In \cref{fig:rubiks_edges}, \edges are visually highlighted in gray.
Each of the \edges appears on two faces.
There are exactly twelve \edges on a \classic.

\paragraph{Center}
\emph{Center} cubies are those which only have one facelet on the outside of \rubiks.
There is exactly one center-cubie on each face which means that we have a total of six center-cubies.
In \cref{fig:rubiks_centers} they are highlighted in gray, while only half of them are visible from the given viewpoint.

\begin{figure}
	\centering
	\begin{minipage}{.2\textwidth}
		\centering
		\input{./Figures/rubiks_cube_faces}
		\caption{Front face.}
		\label{fig:rubiks_faces}
	\end{minipage}
	\hspace{.05\textwidth}
	\begin{minipage}{.2\textwidth}
		\centering
		\input{./Figures/rubiks_cube_corners}
		\caption{\Corners.}
		\label{fig:rubiks_corners}
	\end{minipage}
	\hspace{.05\textwidth}
	\begin{minipage}{.2\textwidth}
		\centering
		\input{./Figures/rubiks_cube_edges}
		\caption{\Edges.}
		\label{fig:rubiks_edges}
	\end{minipage}
	\hspace{.05\textwidth}
	\begin{minipage}{.2\textwidth}
		\centering
		\input{./Figures/rubiks_cube_centers}
		\caption{Center cubies.}
		\label{fig:rubiks_centers}
	\end{minipage}
\end{figure}

\paragraph{Layer}
We define a \emph{layer} to be a subset of cubies that form a plane parallel to a face of the cube.
We denote them with the name of the face and an index subscript.
The faces themselves are annotated with index 0.
In the \classic, a layer is either a face, or the cubies between two faces.
For example we can refer to the front face F as the F$_0$-layer or also the B$_2$-layer.

\paragraph{Size}
The \emph{size} denotes the number of layers in each dimension.
Classical \rubiks{s} have a size of 3, which means that each face has a total of $3^2 = 9$ cubies on it.

%% ----------------------------------------------------------------
\section{Problem Description}
\label{sec:problem}
The cube described above is based on the puzzle invented by Prof.~Ern\~o~Rubik.
Each cubie is of different color on its facelets and when the puzzle is solved, each face has only facelets of one color on it but is colored different than all other faces.
It is possible to independently turn every layer around the cubie in its center by \degs{90}.
Turning any layer of the cube will shift all cubies on that layer and the colors on the faces scramble.
Doing an arbitrary amount of such turns will most likely leave the cube in a state where each face of the cube has a mixture of facelet-colors on it.
Then, the goal is to restore the state of the cube where each face has only facelets of one color on it, as it is shown in \cref{fig:rubiks}.
Recently, it has been proven that any configuration of a \classic can be solved with a maximum of 20 moves~\citep{rokicki2014diameter}, which was already assumed at least 20 years ago~\citep{korf1997finding}.

\subsection{Reducing the Problem Space}
Given their size of 3, \classic{s} consist of $3^3 = 27$ cubies.
However, the one in the center is of no relevance since it is never visible from the outside.
Furthermore, six of the remaining 26 cubies are fixed in their position: The cubies lying in the center of each face cannot be interchanged.
Only turning a middle layer changes the location of such center cubies, which is equivalent to turning the layers parallel to it in the opposite direction.
Also, the locations relative to all other center cubies do not change when applying such an action which is why we forbid turning middle layers.
Although these cubies do rotate when their corresponding faces are rotated they form a fixed reference framework disallowing to turn the entire cube~\citep{korf1997finding}.
We do not have to consider them for solving the puzzle and therefore can narrow it down to a total of 20 cubies necessary for defining our problem space.

From these remaining 20 cubies, eight are \corners and twelve are \edges.
They always stay within their corresponding category, so a \corner can never become an \edge and accordingly the other way around.
Since it is only possible to turn layers by \degs{90}, \corners will always end up in a corner location and \edges accordingly on edge locations.

\subsection{Operators}
\label{subsec:operators}
Given the six faces of a \rubiks, we introduce a total of 18 operations applicable to the problem.
This comes down to three operations per face.
We present them on the front face but apply similarly to the other faces.

The first option is to turn the face by \degs{90} clockwise.
We denote this operation with F, which is an abbreviation for the front face that is turned.
The second possibility is to turn the face by \degs{90} counterclockwise.
We denote this operation with F' (speaking \quotate{F prime}), where the prime implies the opposite direction of the turn.
This is equivalent to three consecutive F operations.
The last possible operation on the is denoted with F2 and describes the move that turns the face upside down, meaning we apply two consecutive F operations, which is also equivalent to doing two F' operations.

Applying the same semantics to the other faces B, L, R, U and D calls for an explanation of the terms clockwise and counterclockwise.
These apply to the direction when looking directly onto the considered face.
For example, using the F operator moves the top left corner on the front face to the top right corner on the front face, whereas using the B operator will move the top left corner on the back face to the bottom left corner on the back face.