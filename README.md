**Domain Abstraction on the Example of the Rubik's Cube**

The work contained in this repository is developed in the context of my Bachelors Thesis at the University of Basel.
It is concerned with *domain abstraction* and evaluates modern heuristics on the domain of the Rubik's Cube.

My work is guided by Patrick Ferber and Jendrik Seipp who work in the *Artificial Intelligence* group at the University of Basel.