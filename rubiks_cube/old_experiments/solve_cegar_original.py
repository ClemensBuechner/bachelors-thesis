#! /usr/bin/env python

import os
import platform

from lab.environments import LocalEnvironment, BaselSlurmEnvironment
from downward.experiment import FastDownwardExperiment

# In the future, these modules should live in a separate
# "planning" or "solver" package.
from downward.reports.absolute import AbsoluteReport

path_to_problem_files = 'problems/'

REMOTE = 'cluster' in platform.node() or 'login-infai' in platform.node()
BENCHMARKS_DIR = os.environ["DOWNWARD_BENCHMARKS"]
if REMOTE:
    ENV = BaselSlurmEnvironment(partition='infai_1', qos='normal', email='clemens.buechner@unibas.ch')
    SUITE = [os.path.abspath(path_to_problem_files + f) for f in os.listdir(path_to_problem_files) if os.path.isfile(os.path.join(path_to_problem_files, f))]
    REPO = '/infai/buecle01/thesis/cegar/cegar-rubiks-cube/'
else:
    ENV = LocalEnvironment(processes=2)
    SUITE = [path_to_problem_files + 'rubikscube_size=3_turns=1_problem=0.sas',
             path_to_problem_files + 'rubikscube_size=3_turns=2_problem=0.sas']
    REPO = '/home/clemi/Switchdrive/uni/lectures/semester6/Bachelorarbeit/cegar/cegar-rubiks-cube/'

ATTRIBUTES = ['coverage', 'expansions_until_last_jump', 'total_time', 'error', 'cost', 'initial_h_value']

# Create a new experiment.
exp = FastDownwardExperiment(environment=ENV)

times = [60, 300, 900]
amounts_of_transitions = [1, 2, 5, 10, 20, 50, 100, 200, 500, 1000]
amounts_of_transitions = [i * 1000000 for i in amounts_of_transitions]
ALGORITHMS = [('cegar-time-{time}'.format(**locals()),
               ['--search', 'astar(cegar(subtasks=[original()], max_states=infinity, max_transitions=infinity, ' +
                'max_time={time}))'.format(**locals())]) for time in times] + \
             [('cegar-transitions-{transitions}'.format(**locals()),
               ['--search', 'astar(cegar(subtasks=[original()], max_states=infinity, max_transitions=' +
                '{transitions}, max_time=infinity))'.format(**locals())]) for transitions in amounts_of_transitions]

REV = 'e2d4c73'
REVNICK = 'latest'

for algo_name, algo_config in ALGORITHMS:
    exp.add_algorithm(REVNICK + ':' + algo_name, REPO, REV, algo_config, driver_options=['--search'])

for task in SUITE:
    exp.add_task(domain='rubiks-cube', problem=os.path.basename(task), problem_file=task)

# Make a report.
exp.add_report(
    AbsoluteReport(attributes=ATTRIBUTES),
    outfile='report.html')

# Parse the commandline and run the specified steps.
exp.run_steps()
