#! /usr/bin/env python

import os
import platform

from lab.environments import LocalEnvironment, BaselSlurmEnvironment
from downward.experiment import FastDownwardExperiment

# In the future, these modules should live in a separate
# "planning" or "solver" package.
from downward.reports.absolute import AbsoluteReport

path_to_problem_files = 'problems/'

REMOTE = 'cluster' in platform.node() or 'login-infai' in platform.node()
BENCHMARKS_DIR = os.environ["DOWNWARD_BENCHMARKS"]
if REMOTE:
    ENV = BaselSlurmEnvironment(partition='infai_1', qos='normal', email='clemens.buechner@unibas.ch')
    SUITE = [os.path.abspath(path_to_problem_files + f) for f in os.listdir(path_to_problem_files) if os.path.isfile(os.path.join(path_to_problem_files, f))]
    REPO = '/infai/buecle01/thesis/cegar/cegar-rubiks-cube/'
else:
    ENV = LocalEnvironment(processes=2)
    SUITE = [path_to_problem_files + 'rubikscube_size=3_turns=1_problem=0.sas',
             path_to_problem_files + 'rubikscube_size=3_turns=2_problem=0.sas']
    REPO = '/home/clemi/Switchdrive/uni/lectures/semester6/Bachelorarbeit/cegar/cegar-sdac/'

ATTRIBUTES = ['coverage', 'expansions_until_last_jump', 'total_time', 'error', 'cost', 'initial_h_value']

# Create a new experiment.
exp = FastDownwardExperiment(environment=ENV)

pattern1 = '[ 0,  1,  2,  3,  4,  5,  6,  7]'  # first half of corner cubies
pattern2 = '[ 8,  9, 10, 11, 12, 13, 14, 15]'  # remaining corner cubies
pattern3 = '[16, 17, 18, 19, 20, 21, 22, 23]'  # first third of edge cubies
pattern4 = '[24, 25, 26, 27, 28, 29, 30, 31]'  # second third of edge cubies
pattern5 = '[32, 33, 34, 35, 36, 37, 38, 39]'  # remaining edge cubies
abstractions = (('[projections(manual_patterns([{pattern1}]), create_complete_transition_system=true),' +
                 ' projections(manual_patterns([{pattern2}]), create_complete_transition_system=true),' +
                 ' projections(manual_patterns([{pattern3}]), create_complete_transition_system=true),' +
                 ' projections(manual_patterns([{pattern4}]), create_complete_transition_system=true),' +
                 ' projections(manual_patterns([{pattern5}]), create_complete_transition_system=true)]').format(**locals()))
ALGORITHMS = [('max', ['--search', 'astar(maximize({abstractions}))'.format(**locals())]),
              ('scp-single', ['--search',
                              'astar(saturated_cost_partitioning({abstractions}, orders=greedy(), max_orders=1))'.format(**locals())]),
              ('blind', ['--search', 'astar(blind())']),
              ('scp-diverse', ['--search', ('astar(saturated_cost_partitioning({abstractions}, orders=greedy(), ' +
                                            'max_orders=infinity, max_time=200, diversify=true, ' +
                                            'max_optimization_time=5))').format(**locals())]),
              ('merge-and-shrink', ['--search', 'astar(merge_and_shrink(' +
                                    'shrink_strategy=shrink_bisimulation(greedy=false),' +
                                    'merge_strategy=merge_sccs(order_of_sccs=topological,' +
                                    'merge_selector=score_based_filtering(scoring_functions=[goal_relevance,dfp,total_order])),' +
                                    'label_reduction=exact(before_shrinking=true,before_merging=false),' +
                                    'max_states=50000,threshold_before_merge=1))'])]

REV = '74b4ec6'  # TODO: Use fixed revision. (replace by hash of revision)
REVNICK = 'latest'

for algo_name, algo_config in ALGORITHMS:
    exp.add_algorithm(REVNICK + ':' + algo_name, REPO, REV, algo_config, driver_options=['--search'])

for task in SUITE:
    exp.add_task(domain='rubiks-cube', problem=os.path.basename(task), problem_file=task)

# Make a report.
exp.add_report(
    AbsoluteReport(attributes=ATTRIBUTES),
    outfile='report.html')

# Parse the commandline and run the specified steps.
exp.run_steps()
