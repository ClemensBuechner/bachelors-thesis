import numpy
import collections
import random


class CubeModel:
    NUMBER_OF_CORNERS = 8
    NUMBER_OF_EDGES = 12

    # The model in this file is not implemented to generate a cube of any dimension. Therefore it is not sufficient
    # to change the DIMENSION variable to get cubes of any dimension.
    DIMENSION = 3

    FACES = ['F', 'B', 'L', 'R', 'U', 'D']

    def __init__(self, size):
        self.size = size
        self.edge_values = {0, size - 1}
        self.center_index = size // 2
        if size % 2 == 0:
            self.center_index = -1

        self.cubies = self.calculate_number_of_cubies()
        self.cube, self.locations = self.initialize_cube()
        self.rotation_indexes, self.rotations = self.initialize_rotations()

    def calculate_number_of_cubies(self):
        cubies = collections.OrderedDict()

        # triples of numbers: (pieces on the cube, possible positions of such pieces on the cube, possible rotations)
        cubies['corner'] = (self.NUMBER_OF_CORNERS, self.NUMBER_OF_CORNERS, 3)
        cubies['edge'] = (self.NUMBER_OF_EDGES * (self.size % 2), self.NUMBER_OF_EDGES, 2)
        cubies['wing'] = (self.NUMBER_OF_EDGES * (self.size // 2 - 1) * 2, self.NUMBER_OF_EDGES * 2, 1)
        cubies['facediagonal'] = (len(self.FACES) * (self.size // 2 - 1) * 4, len(self.FACES) * 4, 1)
        cubies['facecenter'] = (len(self.FACES) * (self.size // 2 - 1) * 4 * (self.size % 2), len(self.FACES) * 4, 1)
        cubies['facewing'] = (len(self.FACES) * (self.size // 2 - 2) * (self.size // 2 - 1) * 4, len(self.FACES) * 8, 1)

        return cubies

    def initialize_cube(self):
        cube = numpy.zeros((self.size, self.size, self.size), int) - 1
        locations = numpy.zeros((self.size, self.size, self.size), int) - 1

        variable_counter = 0

        for key in self.cubies.keys():
            pieces, positions, rotations = self.cubies[key]
            if key == 'facewing':
                max_index = self.size // 2 + 1
                for i in range(1, max_index):
                    for j in range(i + 1, max_index):
                        location_counter = 0
                        for x in range(self.size):
                            for y in range(self.size):
                                for z in range(self.size):
                                    if cube[x, y, z] < 0 and self.is_facewing(x, y, z, i, j):
                                        cube[x, y, z] = variable_counter
                                        variable_counter += 1
                                        locations[x, y, z] = location_counter
                                        location_counter += 1
            else:
                for i in range(1, pieces // positions + 1):
                    location_counter = 0
                    for x in range(self.size):
                        for y in range(self.size):
                            for z in range(self.size):
                                if cube[x, y, z] < 0:
                                    if key == 'corner' and self.is_corner(x, y, z):
                                        cube[x, y, z] = variable_counter
                                        variable_counter += 2
                                        locations[x, y, z] = location_counter
                                        location_counter += 1
                                    elif key == 'edge' and self.is_edge(x, y, z):
                                        cube[x, y, z] = variable_counter
                                        variable_counter += 2
                                        locations[x, y, z] = location_counter
                                        location_counter += 1
                                    elif key == 'wing' and self.is_wing(x, y, z, i):
                                        cube[x, y, z] = variable_counter
                                        variable_counter += 1
                                        locations[x, y, z] = location_counter
                                        location_counter += 1
                                    elif key == 'facediagonal' and self.is_facediagonal(x, y, z, i):
                                        cube[x, y, z] = variable_counter
                                        variable_counter += 1
                                        locations[x, y, z] = location_counter
                                        location_counter += 1
                                    elif key == 'facecenter' and self.is_facecenter(x, y, z, i):
                                        cube[x, y, z] = variable_counter
                                        variable_counter += 1
                                        locations[x, y, z] = location_counter
                                        location_counter += 1
        return cube, locations

    def initialize_rotations(self):
        n = 2 + (self.size % 2)
        indexes = numpy.zeros((n, n, n), int) - 1
        rotation_values = numpy.zeros((n, n, n), dtype=(int, self.DIMENSION))

        variable_counter = 1
        edge_values = {0, n - 1}

        for key in self.cubies:
            pieces, positions, rotations = self.cubies[key]
            if rotations > 1:
                for x in range(n):
                    for y in range(n):
                        for z in range(n):
                            if indexes[x, y, z] < 0:
                                if key == 'corner' and x in edge_values and y in edge_values and z in edge_values:
                                    indexes[x, y, z] = variable_counter
                                    variable_counter += 2
                                    if ((x == n - 1) + (y == n - 1) + (z == n - 1)) % 2 == 1:
                                        rotation_values[x, y, z] = (0, 2, 1)
                                    else:
                                        rotation_values[x, y, z] = (0, 1, 2)
                                elif key == 'edge':
                                    if x in edge_values and y in edge_values and z == 1:
                                        rotation_values[x, y, z] = (0, 1, -1)
                                        indexes[x, y, z] = variable_counter
                                        variable_counter += 2
                                    elif x in edge_values and z in edge_values and y == 1:
                                        rotation_values[x, y, z] = (0, -1, 1)
                                        indexes[x, y, z] = variable_counter
                                        variable_counter += 2
                                    elif y in edge_values and z in edge_values and x == 1:
                                        rotation_values[x, y, z] = (-1, 0, 1)
                                        indexes[x, y, z] = variable_counter
                                        variable_counter += 2

        return indexes, rotation_values

    def rotate_cubie(self, rotation, axis1, axis2):
        rotation[axis1], rotation[axis2] = rotation[axis2], rotation[axis1]
        return rotation

    def turn_clockwise(self, face, index, turns):
        if index > self.size // 2:
            print('Unknown rotation operation.\n')
            return

        rotation_indexes = range(self.size % 2 + 2)

        if face in {'B', 'R', 'D'}:
            index = self.size - 1 - index
        if face in {'B', 'L', 'D'}:
            turns = -turns

        _x = rotation_indexes
        _y = rotation_indexes
        _z = rotation_indexes
        axis1, axis2 = -1, -1

        if face in {'F', 'B'}:
            _x = [rotation_indexes[-(face == 'B')]]
            axis1, axis2 = 1, 2
        elif face in {'L', 'R'}:
            _y = [rotation_indexes[-(face == 'R')]]
            axis1, axis2 = 0, 2
        elif face in {'U', 'D'}:
            _z = [rotation_indexes[-(face == 'D')]]
            axis1, axis2 = 0, 1

        if turns % 2 != 0 and index in self.edge_values:
            for x in _x:
                for y in _y:
                    for z in _z:
                        self.rotations[x, y, z] = self.rotate_cubie(self.rotations[x, y, z], axis1, axis2)

        if face in {'F', 'B'}:
            x = _x[0]
            self.cube[index, :, :] = numpy.rot90(self.cube[index, :, :], turns)
            self.rotations[x, :, :] = numpy.rot90(self.rotations[x, :, :], turns)
            self.rotation_indexes[x, :, :] = numpy.rot90(self.rotation_indexes[x, :, :], turns)
        elif face in {'L', 'R'}:
            y = _y[0]
            self.cube[:, index, :] = numpy.rot90(self.cube[:, index, :], turns)
            self.rotations[:, y, :] = numpy.rot90(self.rotations[:, y, :], turns)
            self.rotation_indexes[:, y, :] = numpy.rot90(self.rotation_indexes[:, y, :], turns)
        elif face in {'U', 'D'}:
            z = _z[0]
            self.cube[:, :, index] = numpy.rot90(self.cube[:, :, index], turns)
            self.rotations[:, :, z] = numpy.rot90(self.rotations[:, :, z], turns)
            self.rotation_indexes[:, :, z] = numpy.rot90(self.rotation_indexes[:, :, z], turns)

    def shuffle(self, turns, turn_options):
        max_index = self.size // 2 - 1
        string = ''
        last_face = None

        for i in range(turns):
            while True:
                face = random.choice(self.FACES)
                if face != last_face:
                    last_face = face
                    break
            index = random.randint(0, max_index)
            turns = random.choice(turn_options)
            string += str(face)
            if turns == 2:
                string += str(2)
            elif turns == 3:
                string += '\''
            string += '_' + str(index) + ' '
            self.turn_clockwise(face, index, turns)

        return string

    def controlled_shuffle(self, scramble):
        scramble = scramble.split(' ')
        for step in scramble:
            s = step.split('_')
            if self.size < 4:
                index = 0
            else:
                index = int(s[1])
            face = s[0][:1]
            if s[0][1:2] == '2':
                turns = 2
            elif s[0][1:2] == '\'':
                turns = 3
            else:
                turns = 1

            self.turn_clockwise(face, index, turns)

    def get_cubies(self):
        return self.cubies

    def get_cube(self):
        return self.cube

    def get_value(self, var_index):
        if var_index < (self.NUMBER_OF_CORNERS + self.NUMBER_OF_EDGES * (self.size % 2)) * 2 and var_index % 2 == 1:
            n = 2 + (self.size % 2)
            for x in range(n):
                for y in range(n):
                    for z in range(n):
                        if self.rotation_indexes[x, y, z] == var_index:
                            for i in range(self.DIMENSION):
                                if self.rotations[x, y, z][i] >= 0:
                                    return self.rotations[x, y, z][i]
        else:
            for x in range(self.size):
                for y in range(self.size):
                    for z in range(self.size):
                        if self.cube[x, y, z] == var_index:
                            return int(self.locations[x, y, z])
        return None

    def get_rotation(self, var_index):
        if var_index < (self.NUMBER_OF_CORNERS + self.NUMBER_OF_EDGES * (self.size % 2)) * 2 and var_index % 2:
            n = 2 + (self.size % 2)
            for x in range(n):
                for y in range(n):
                    for z in range(n):
                        if self.rotation_indexes[x, y, z] == var_index:
                            return self.rotations[x, y, z]
        return None

    def rotate_single_cubie(self, var_index):
        if var_index < (self.NUMBER_OF_CORNERS + self.NUMBER_OF_EDGES * (self.size % 2)) * 2 and var_index % 2 == 1:
            n = 2 + (self.size % 2)
            edge_values = {0, n - 1}
            for x in range(n):
                for y in range(n):
                    for z in range(n):
                        if self.rotation_indexes[x, y, z] == var_index:
                            if x in edge_values and y in edge_values and z in edge_values:
                                _x, _y, _z = self.rotations[x, y, z]
                                self.rotations[x, y, z] = (_y, _z, _x)
                            else:
                                for i in range(self.DIMENSION):
                                    if self.rotations[x, y, z][i] < 0:
                                        self.rotations[x, y, z][i - 1], self.rotations[x, y, z][(i + 1) % self.DIMENSION] = \
                                            self.rotations[x, y, z][(i + 1) % self.DIMENSION], self.rotations[x, y, z][i - 1]
                            return

    def location_on_face(self, var_location, face, index, key, key_index1, key_index2):
        if face in {'B', 'R', 'D'}:
            index = self.size - 1 - index

        for x in range(self.size):
            for y in range(self.size):
                for z in range(self.size):
                    if self.locations[x, y, z] == var_location:
                        if key == 'corner' and self.is_corner(x, y, z):
                            return (face in {'F', 'B'} and x == index) or \
                                   (face in {'L', 'R'} and y == index) or \
                                   (face in {'U', 'D'} and z == index)
                        elif key == 'edge' and self.is_edge(x, y, z):
                            return (face in {'F', 'B'} and x == index) or \
                                   (face in {'L', 'R'} and y == index) or \
                                   (face in {'U', 'D'} and z == index)
                        elif key == 'wing' and self.is_wing(x, y, z, key_index1):
                            return (face in {'F', 'B'} and x == index) or \
                                   (face in {'L', 'R'} and y == index) or \
                                   (face in {'U', 'D'} and z == index)
                        elif key == 'facediagonal' and self.is_facediagonal(x, y, z, key_index1):
                            return (face in {'F', 'B'} and x == index) or \
                                   (face in {'L', 'R'} and y == index) or \
                                   (face in {'U', 'D'} and z == index)
                        elif key == 'facecenter' and self.is_facecenter(x, y, z, key_index1):
                            return (face in {'F', 'B'} and x == index) or \
                                   (face in {'L', 'R'} and y == index) or \
                                   (face in {'U', 'D'} and z == index)
                        elif key == 'facewing' and self.is_facewing(x, y, z, key_index1, key_index2):
                            return (face in {'F', 'B'} and x == index) or \
                                   (face in {'L', 'R'} and y == index) or \
                                   (face in {'U', 'D'} and z == index)

    def is_corner(self, x, y, z):
        x_on_face = x in self.edge_values
        y_on_face = y in self.edge_values
        z_on_face = z in self.edge_values

        return x_on_face and y_on_face and z_on_face

    def is_edge(self, x, y, z):
        x_on_face = x in self.edge_values
        y_on_face = y in self.edge_values
        z_on_face = z in self.edge_values

        return (x_on_face and y_on_face and z == self.center_index) or \
               (x_on_face and z_on_face and y == self.center_index) or \
               (y_on_face and z_on_face and x == self.center_index)

    def is_wing(self, x, y, z, index):
        return (x in self.edge_values and y in self.edge_values and index == min(z, self.size - z - 1)) or \
               (x in self.edge_values and z in self.edge_values and index == min(y, self.size - y - 1)) or \
               (y in self.edge_values and z in self.edge_values and index == min(x, self.size - x - 1))

    def is_facediagonal(self, x, y, z, index):
        return (x in self.edge_values and (y == z or y == self.size - z - 1) and index == min(y, self.size - y - 1)) or \
               (y in self.edge_values and (x == z or x == self.size - z - 1) and index == min(x, self.size - x - 1)) or \
               (z in self.edge_values and (x == y or x == self.size - y - 1) and index == min(x, self.size - x - 1))

    def is_facecenter(self, x, y, z, index):
        return (x in self.edge_values and (y != z) and ((y == self.center_index and index == min(z, self.size - z - 1)) or (z == self.center_index and index == min(y, self.size - y - 1)))) or \
               (y in self.edge_values and (x != z) and ((x == self.center_index and index == min(z, self.size - z - 1)) or (z == self.center_index and index == min(x, self.size - x - 1)))) or \
               (z in self.edge_values and (x != y) and ((x == self.center_index and index == min(y, self.size - y - 1)) or (y == self.center_index and index == min(x, self.size - x - 1))))

    def is_facewing(self, x, y, z, index1, index2):
        i = {index1, self.size - 1 - index1}
        j = {index2, self.size - 1 - index2}
        return (x in self.edge_values and ((y in i and z in j) or (y in j and z in i))) or \
               (y in self.edge_values and ((x in i and z in j) or (x in j and z in i))) or \
               (z in self.edge_values and ((x in i and y in j) or (x in j and y in i)))

    def printing(self, name):
        if name == 'cube':
            print(self.cube)
        elif name == 'locations':
            print(self.locations)
        elif name == 'rotations':
            print(self.rotations)
        else:
            print(self.rotation_indexes)
