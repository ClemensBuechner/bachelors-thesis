#! /usr/bin/env python

import os
import platform

from lab.environments import LocalEnvironment, BaselSlurmEnvironment
from downward.experiment import FastDownwardExperiment

# In the future, these modules should live in a separate
# "planning" or "solver" package.
from downward.reports.absolute import AbsoluteReport
from downward.reports.scatter import ScatterPlotReport

path_to_problem_files = '../problems/'

REMOTE = 'cluster' in platform.node() or 'login-infai' in platform.node()
BENCHMARKS_DIR = os.environ["DOWNWARD_BENCHMARKS"]
if REMOTE:
    ENV = BaselSlurmEnvironment(partition='infai_1', qos='normal', email='clemens.buechner@unibas.ch')
    SUITE = [os.path.abspath(path_to_problem_files + f) for f in os.listdir(path_to_problem_files) if os.path.isfile(os.path.join(path_to_problem_files, f))]
    REPO = '/infai/buecle01/thesis/cegar/cegar-rubiks-cube/'
else:
    ENV = LocalEnvironment(processes=2)
    SUITE = [path_to_problem_files + 'rubikscube_size=3_turns=1_problem=0.sas',
             path_to_problem_files + 'rubikscube_size=3_turns=2_problem=0.sas',
             path_to_problem_files + 'rubikscube_size=3_turns=5_problem=0.sas']
    REPO = '/home/clemi/Switchdrive/uni/lectures/semester6/Bachelorarbeit/cegar/cegar-rubiks-cube/'

ATTRIBUTES = ['coverage', 'expansions_until_last_jump', 'total_time', 'error', 'cost', 'initial_h_value']

# Create a new experiment.
exp = FastDownwardExperiment(environment=ENV)
#parser = os.path.abspath(os.path.join(__file__, 'parser/start_time-parser.py'))
#exp.add_parser(parser)

ALGORITHMS = [('cegar', ['--search', 'astar(cegar(subtasks=[original()], max_states=infinity, ' +
                         'max_transitions=5000000, max_time=infinity))'])]

REV = ['c38a181', '0e982f2', '4a11c86', 'c8cb592']
REVNICK = ['end_of_thesis', 'optimized_regress', 'precomp_fact_eff_pairs', 'referencing_precomp']

for i in range(len(REV)):
    exp.add_algorithm(REVNICK[i] + ':' + ALGORITHMS[0][0], REPO, REV[i], ALGORITHMS[0][1], driver_options=['--search'])

for task in SUITE:
    exp.add_task(domain='rubiks-cube', problem=os.path.basename(task), problem_file=task)

# Make a report.
exp.add_report(
    AbsoluteReport(attributes=ATTRIBUTES),
    outfile='report.html')

# Parse the commandline and run the specified steps.
exp.run_steps()
