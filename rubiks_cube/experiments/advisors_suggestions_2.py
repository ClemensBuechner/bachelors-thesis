#! /usr/bin/env python

import os
import platform

from lab.environments import LocalEnvironment, BaselSlurmEnvironment
from downward.experiment import FastDownwardExperiment

# In the future, these modules should live in a separate
# "planning" or "solver" package.
from downward import suites
from downward.reports.absolute import AbsoluteReport
from downward.reports.scatter import ScatterPlotReport

path_to_problem_files = os.path.dirname(os.path.abspath(__file__)) + '/../problems/'

NODE = platform.node()
REMOTE = NODE.endswith(".scicore.unibas.ch") or NODE.endswith(".cluster.bc2.ch")
BENCHMARKS_DIR = os.environ["DOWNWARD_BENCHMARKS"]
if REMOTE:
    ENV = BaselSlurmEnvironment(partition='infai_1', qos='normal', 
                                email="clemens.buechner@unibas.ch")
    SUITE = [os.path.abspath(path_to_problem_files + f) 
             for f in os.listdir(path_to_problem_files) 
             if os.path.isfile(os.path.join(path_to_problem_files, f))]
    REPO = '/infai/buecle01/thesis/cegar/cegar-rubiks-cube/'
else:
    ENV = LocalEnvironment(processes=2)
    SUITE = [path_to_problem_files + 'rubikscube_size=3_turns=1_problem=0.sas',
             path_to_problem_files + 'rubikscube_size=3_turns=2_problem=0.sas',
             path_to_problem_files + 'rubikscube_size=3_turns=5_problem=0.sas']
    REPO = '/home/clemi/Switchdrive/uni/lectures/semester6/Bachelorarbeit/cegar/cegar-rubiks-cube/'

ATTRIBUTES = ['coverage', 'expansions_until_last_jump', 'total_time', 'error',
              'cost', 'initial_h_value', 'search_start_time',
              'search_start_memory']

korf_patterns = '[[0, 1, 2, 3, 4, 5, 6, 7], [8, 9, 10, 11, 12, 13], [14, 15, 16, 17, 18, 19]]'
corner_edge_patterns = '[[0, 1, 2, 3, 4, 5, 6, 7], [8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19]]'

# Create a new experiment.
exp = FastDownwardExperiment(environment=ENV)
exp.add_parser(exp.PLANNER_PARSER)
exp.add_parser(exp.EXITCODE_PARSER)
exp.add_parser(exp.SINGLE_SEARCH_PARSER)
exp.add_parser(os.path.dirname(os.path.abspath(__file__)) + '/parser/times-parser.py')

ALGORITHMS = [
    ('cegar-korf-patterns-scp', ['--search', ('astar(cegar([variables(patterns=manual_patterns({korf_patterns}))], ' +
                                              'max_transitions=infinity))').format(**locals())]),
    ('cegar-korf-patterns-max', ['--search', ('astar(maximize([cartesian([variables(patterns=manual_patterns(' +
                                              '{korf_patterns}))], max_transitions=infinity))))').format(**locals())]),
    ('cegar-korf-patterns-max-900s', ['--search', ('astar(maximize([cartesian([variables(patterns=manual_patterns(' +
                                                   '{korf_patterns}))], max_transitions=infinity, ' +
                                                   'max_time=900))))').format(**locals())]),
    ('cegar-corner-edge-patterns-max-900s', ['--search', ('astar(maximize([cartesian([variables(' +
                                                          'patterns=manual_patterns({corner_edge_patterns}))], ' +
                                                          'max_transitions=infinity, ' +
                                                          'max_time=900))))').format(**locals())])
             ]

REVISIONS = [('69bf072', 'max_time_param')]

for rev, rev_nick in REVISIONS:
    for algo_name, algo_config in ALGORITHMS:
        exp.add_algorithm(rev_nick + ':' + algo_name, REPO, rev,
                          algo_config, driver_options=['--search'])

for task in SUITE:
    exp.add_task(domain='rubiks-cube', problem=os.path.basename(task),
                 problem_file=task)

# Add step that writes experiment files to disk.
exp.add_step('build', exp.build)

# Add step that executes all runs.
exp.add_step('start', exp.start_runs)

# Add step that collects properties from run directories and
# writes them to *-eval/properties.
exp.add_fetcher(name='fetch')

# Make a report.
exp.add_report(
    AbsoluteReport(attributes=ATTRIBUTES),
    outfile='report.html')

# Parse the commandline and run the specified steps.
exp.run_steps()
