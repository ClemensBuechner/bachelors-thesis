#! /usr/bin/env python

import os
import platform

from lab.environments import LocalEnvironment, BaselSlurmEnvironment
from downward.experiment import FastDownwardExperiment
from downward.experiment import Experiment

# In the future, these modules should live in a separate
# "planning" or "solver" package.
from downward.reports.absolute import AbsoluteReport
from downward.reports.scatter import ScatterPlotReport

path_to_problem_files = 'problems/'

REMOTE = 'cluster' in platform.node() or 'login-infai' in platform.node()
BENCHMARKS_DIR = os.environ["DOWNWARD_BENCHMARKS"]
if REMOTE:
    path_to_reports = '/infai/buecle01/thesis/repo/rubiks_cube/data/'
else:
    path_to_reports = '/home/clemi/Switchdrive/uni/lectures/semester6/Bachelorarbeit/repo/evaluations/'

ATTRIBUTES = ['coverage', 'expansions_until_last_jump', 'total_time', 'error', 'cost', 'initial_h_value']

# Collect reports to put into one report.
exp = Experiment()
exp.add_fetcher(path_to_reports + 'final_experiments-eval/')
exp.add_fetcher(path_to_reports + 'updated_final_experiments-eval/')

exp.add_report(AbsoluteReport(attributes=ATTRIBUTES, filter_algorithm=['latest:6_cegar', 'optimized_regress:cegar']))

# Parse the commandline and run the specified steps.
exp.run_steps()
